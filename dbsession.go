package ksqlttp

import (
	"database/sql"
	"errors"
	"log"
	"regexp"
	"time"
	"fmt"

	"github.com/google/uuid"
	_ "github.com/mattn/go-sqlite3"
)

type DbSession struct {
	sid uuid.UUID
	sdb *sql.DB

	tborn time.Time
	tlast time.Time
}

type DbResult struct {
	Err    error
	Names  []string
	Types  []string
	Values []string
}

//var alphanumeric = regexp.MustCompile("^[a-zA-Z0-9_.@+/\\-:]*$")

func isAlphanumeric(txt string) bool {
	var alphanumeric = regexp.MustCompile("^[a-zA-Z0-9]*$")

	return alphanumeric.MatchString(txt)
}

func isValidName(txt string) bool {
	var alphanumeric = regexp.MustCompile("^[a-zA-Z0-9_-]*$")

	return alphanumeric.MatchString(txt)
}

func NewDbSession(path string) *DbSession {
	session := new(DbSession)

	session.sid = uuid.New()

	db, err := sql.Open("sqlite3", path)

	if err != nil {
		log.Printf("%q: %s\n", err, "Failed open sql database.")

		return nil
	}

	session.sdb = db
	session.tborn = time.Now()
	session.tlast = session.tborn

	return session
}

func (self *DbSession) Close() {
	self.sdb.Close()
	self.sdb = nil
	self.sid = uuid.Nil
}

func (self *DbSession) get_types(container string) map[string]string {
	types_ := map[string]string{}

	request := "pragma table_info(" + container + ");"

	log.Println("Get types request: ", request)

	rows, err := self.sdb.Query(request)

	if err != nil {
		log.Println("Get types error: ", err.Error())
		return nil
	}

	defer rows.Close()

	for rows.Next() {
		var cid int
		var name string
		var ttype string
		var nnull int
		var value sql.NullString
		var pk int

		err = rows.Scan(&cid, &name, &ttype, &nnull, &value, &pk)

		if err != nil {
			log.Println("Get types error: ", err.Error())
			return nil
		}

		log.Println("Get types name", name, "type", ttype)

		types_[name] = ttype
	}

	return types_
}

func (self *DbSession) is_text_type(type_ string) bool {
	if type_ == "TEXT" || type_ == "VARCHAR" || type_ == "NVARCHAR" || type_ == "TIMESTAMP"{
		return true
	}

	return false
}

func (self *DbSession) if_text(val string, vtype string) string {
	if !self.is_text_type(vtype) {
		return val
	}

	return "'" + val + "'"
}

func (self *DbSession) GetValues(container string, vals []string, keys map[string]string) *DbResult {
	log.Println("Get values request: vals>", vals, " keys>", keys)

	result := new(DbResult)
	result.Err = nil
	result.Names = nil
	result.Types = nil
	result.Values = nil

	types := self.get_types(container)

	if len(vals) < 1 || len(keys) < 1 {
		result.Err = errors.New("Invalid key/value count.")
		return result
	}

	var columns_ string

	for i, v := range vals {
		if !isValidName(v) {
			result.Err = errors.New("Invalid value name.")
			return result
		} else {
			columns_ += (v)

			if i != (len(vals) - 1) {
				columns_ += ", "
			}
		}
	}

	var where_ string

	i := 0
	for k, v := range keys {
		if !isValidName(k) {
			result.Err = errors.New("Invalid keys.")
			return result
		} else {
			v = self.if_text(v, types[k])
			where_ += (k + "=" + v)

			if i != (len(keys) - 1) {
				where_ += " AND "
			}
		}
		i++
	}

	var request string

  var count int = 0

  request = `select count(` + vals[0] + `) from ` + container + ` where ` + where_ + `;`

	log.Println("Get values rows count request: ", request)

  _ = self.sdb.QueryRow(request).Scan(&count)

  if count < 1 {
    result.Err = errors.New("Request have no result.")
    return result
  } else {
    log.Println("Get values rows count is: ", count)
  }

	request = "SELECT " + columns_ + " FROM " + container + " WHERE " + where_ + ";"

	log.Println("Get values request: ", request)

	rows, err := self.sdb.Query(request)
  defer rows.Close()

	if err != nil {
    log.Println("Get values row error: ", err.Error())
		result.Err = err
		return result
	}

  cols, err := rows.Columns()

	if err != nil {
    log.Println("Get values cols error: ", err.Error())
		result.Err = err
		return result
	}

  if len(cols) < 1 {
    err := errors.New("No columns.")
    log.Println("Get values cols error: ", err.Error())
		result.Err = err
		return result
  } else {
    log.Println("Get values request columns: ", cols)
  }

	pointers_ := make([]interface{}, len(vals))
	container_ := make([]interface{}, len(vals))
	values_ := make([]string, 0)

	for i, _ := range pointers_ {
		pointers_[i] = &container_[i]
	}


  for rows.Next() {
    err := rows.Scan(pointers_...)

    if err != nil {
      log.Println("Get values scan error: ", err.Error())
      result.Err = err
      return result
    }

    for i, v := range(container_) {
      var vv string
      switch v.(type) {
      case nil:
        vv = ""
      default:
        vv = fmt.Sprintf("%v", v)
      }
      log.Printf("Type of column is %v %T", i, v)
      values_ = append(values_, vv)
    }
  }


	log.Println("Scan rows ", container_)
	log.Println("Collect values ", values_)

	result.Names = vals
	result.Values = values_

	return result
}

func (self *DbSession) SetValues(container string, vals map[string]string, keys map[string]string) *DbResult {
	log.Println("Set values request: vals>", vals, " keys>", keys)

	result := new(DbResult)
	result.Err = nil
	result.Names = nil
	result.Types = nil
	result.Values = nil

	if len(vals) < 1 {
		result.Err = errors.New("Invalid values count.")
		return result
	}

	var insert_ bool

	insert_ = true

	var wherek_ []string
	var wherev_ []string

	if len(keys) > 0 {
		insert_ = false
	}

	log.Println("Set values request: insert>", insert_, len(keys))

	if insert_ == false {
		for k, v := range keys {
			if !isValidName(k) {
				result.Err = errors.New("Invalid keys.")

				return result
			} else {
				wherek_ = append(wherek_, k)
				wherev_ = append(wherev_, v)
			}
		}
	}

	var columns_ []string
	var values_ []string

	for k, v := range vals {
		if !isValidName(k) {
			result.Err = errors.New("Invalid values.")

			return result
		} else {
			columns_ = append(columns_, k)
			values_ = append(values_, v)
		}
	}

	log.Println("Set values request: columns_>", columns_, " values_>", values_)

	types_ := self.get_types(container)

	log.Println("Set values request: types_>", types_)

	var request string

	if insert_ {
		request = "INSERT INTO " + container + "  ("
		for i, v := range columns_ {

			request += v

			if i != (len(columns_) - 1) {
				request += ","
			}
		}

		request += ") VALUES ("

		for i, v := range values_ {
			request += self.if_text(v, types_[columns_[i]])

			if i != (len(values_) - 1) {
				request += ","
			}
		}

		request += ");"
	} else {
		request = "UPDATE " + container + " SET "

		for i, v := range columns_ {
			request += (v + "=" + self.if_text(values_[i], types_[v]))

			if i != (len(columns_) - 1) {
				request += ","
			}
		}

		request += " WHERE "

		for i, v := range wherek_ {
			request += v + "=" + self.if_text(wherev_[i], types_[v])

			if i != (len(wherek_) - 1) {
				request += " AND "
			}
		}

		request += ";"
	}

	log.Println("Set values request: ", request)

	_, err := self.sdb.Exec(request)

	if err != nil {
		log.Println("Set values request error: ", err.Error())
	}

	result.Err = err

	return result
}

func (self *DbSession) DelValues(container string, vals []string, keys map[string]string) *DbResult {
	log.Println("Del values request: vals>", vals, " keys>", keys)

	result := new(DbResult)
	result.Err = nil
	result.Names = nil
	result.Types = nil
	result.Values = nil

	var wherek_ []string
	var wherev_ []string

	for k, v := range keys {
		if !isValidName(k) {
			result.Err = errors.New("Invalid keys.")

			return result
		} else {
			wherek_ = append(wherek_, k)
			wherev_ = append(wherev_, v)
		}
	}

	var request string

	request = "DELETE FROM " + container + " WHERE "

	types_ := self.get_types(container)

	for i, v := range wherek_ {
		request += v + "=" + self.if_text(wherev_[i], types_[v])

		if i != (len(wherek_) - 1) {
			request += " AND "
		}
	}

	request += ";"

	log.Println("Del values request: ", request)

	_, err := self.sdb.Exec(request)

	if err != nil {
		log.Println("Del values request error: ", err.Error())
	}

	result.Err = err

	return result
}

func (self *DbSession) HasValues(container string, vals []string, keys map[string]string) *DbResult {
	log.Println("Has values request: vals>", vals, " keys>", keys)

	result := new(DbResult)
	result.Err = nil
	result.Names = nil
	result.Types = nil
	result.Values = nil

	var wherek_ []string
	var wherev_ []string

	for k, v := range keys {
		if !isValidName(k) {
			result.Err = errors.New("Invalid keys.")

			return result
		} else {
			wherek_ = append(wherek_, k)
			wherev_ = append(wherev_, v)
		}
	}

	types_ := self.get_types(container)

	var request string

  var count int = 0

  request = `select count(*) from ` + container + ` where `

	for i, v := range wherek_ {
		request += v + "=" + self.if_text(wherev_[i], types_[v])

		if i != (len(wherek_) - 1) {
			request += " AND "
		}
	}

  request +=  `;`

	log.Println("Has values rows count request: ", request)

  _ = self.sdb.QueryRow(request).Scan(&count)

  if count < 1 {
    result.Err = errors.New("Request have no result.")
    return result
  } else {
    log.Println("Has values rows count is: ", count)
  }

	request = "SELECT "

	for i, v := range wherek_ {
		request += v

		if i != (len(wherek_) - 1) {
			request += ","
		}
	}

	request += " FROM " + container + " WHERE "

	for i, v := range wherek_ {
		request += v + "=" + self.if_text(wherev_[i], types_[v])

		if i != (len(wherek_) - 1) {
			request += " AND "
		}
	}

	request += ";"

	log.Println("Has values request: ", request)

	row := self.sdb.QueryRow(request)

	if row.Err() != nil {
		log.Println("Has values request error: ", row.Err().Error())

		result.Err = row.Err()

		return result
	}

	var data string

	err := row.Scan(&data)

	if err != nil && err == sql.ErrNoRows {
		log.Println("Has values request rows error: ", err.Error())

		result.Err = err

		return result
	} else {

	}

	return result
}
