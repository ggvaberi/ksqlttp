module gitlab.com/ggvaberi/ksqlttp

go 1.19

require (
	github.com/google/uuid v1.3.0
	github.com/mattn/go-sqlite3 v1.14.17
	gopkg.in/gcfg.v1 v1.2.3
)

require gopkg.in/warnings.v0 v0.1.2 // indirect
