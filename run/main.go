package main

import (
	"encoding/base64"
	"encoding/json"
	"io"
	"io/ioutil"

	"log"
	"net/http"
	"net"
	"os"
	"os/signal"
	"runtime/pprof"
	"time"

	"github.com/google/uuid"
	"gitlab.com/ggvaberi/ksqlttp"
	"gopkg.in/gcfg.v1"
)

type Config struct {
	Main struct {
		DbPath  string
		Debug   bool
    Tlskey  string
    Tlscert string
	}
}

type KGetValues struct {
	Container string
	Values    []string
	Keys      map[string]string
}

type KHasValues struct {
	Container string
	Values    []string
	Keys      map[string]string
}

type KDelValues struct {
	Container string
	Values    []string
	Keys      map[string]string
}

type KSetValues struct {
	Container string
	Values    map[string]string
	Keys      map[string]string
}

type KDbOpen struct {
	Name string
	Pass string
}

var dbsession *ksqlttp.DbSession = nil

var resdebug = false
var locdebug = true

func getValue(r *http.Request, key string) string {
	keys, ok := r.URL.Query()[key]

	if !ok || len(keys[0]) < 1 {
		log.Printf("Error: Url query key " + key + " is missing.")

		return ""
	}

	return keys[0]
}

func getCookie(r *http.Request, key string) string {
  if r == nil || key == "" {
    return ""
  }

  for _, c := range r.Cookies() {
		if c.Name == key {
      return c.Value
    }
  }

  return ""
}

func hostOnly(addr string) string {
	log.Println("hostOnly: ", addr)

	var host string = ""

	host, _, err := net.SplitHostPort(addr)

	if err != nil || host == "::1" {
		return "127.0.0.1"
	}

	return host
}

func isSessionValid(sid string, host string) bool {
  if sid == "" {
		log.Printf("Error while check session validity:", "Invalid sid content.")
		return false
  }

  res := dbsession.HasValues("db_session", []string{"id"}, map[string]string{"closed" : "0", "sid" : sid, "remote" : host})

  if res.Err != nil {
		log.Printf("Error while check session validity:", res.Err.Error())
		return false
  }

  return true
}

func responceError(w *http.ResponseWriter, title string, msg string) {
	log.Println("Error:", title, msg)

	var res string

	if resdebug {
		res = "{ \"error\": \"" + title + " " + msg + "\" }"
	} else {
		title = "Internal error."
		res = "{ \"error\": \"Internal error.\" }"
	}

	//(*w).WriteHeader(http.StatusForbidden)
	(*w).Header().Set("Content-Type", "application/json")
	http.Error(*w, res, http.StatusInternalServerError)
	//io.WriteString(*w, res)
}

func handleIndex(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "ERROR!\n")
}

func handleDbOpen(w http.ResponseWriter, r *http.Request) {
	var t KDbOpen
	var p []byte
	var err error

	p, err = ioutil.ReadAll(r.Body)

	if err != nil {
		responceError(&w, "DbOpen body", err.Error())
		return
	}

	log.Printf("BODY: %q", p)

	err = json.Unmarshal(p, &t)

	if err != nil {
		responceError(&w, "DbOpen unmarshal", err.Error())
		return
	}

	pass, err := base64.StdEncoding.DecodeString(t.Pass)

	t.Pass = string(pass[:])

	log.Printf("User: %s", t.Name)
	log.Printf("Pass: %s", t.Pass)

	//keys := fmt.Sprintf("name=%s,pass=%s", t.Name, t.Pass)

	res := dbsession.HasValues("db_admins", []string{}, map[string]string{"name": t.Name, "pass": t.Pass})

	if res.Err != nil {
		responceError(&w, "DbOpen check admin", res.Err.Error())
		return
	}

	//keys = fmt.Sprintf("uid=0,closed=0")

	res = dbsession.GetValues("db_session", []string{"sid"}, map[string]string{"uid": "0", "closed": "0", "remote" :  hostOnly(r.RemoteAddr)})

	sid := uuid.New().String()

  log.Println("DB open res len: ", len(res.Values))

	if res.Err != nil || res.Values[0] == "" {
		log.Println("Db Open Error: ", "Session search>", res.Err.Error())

		//vals := fmt.Sprintf("uid=0,closed=0,sid=%s,remote=%s", sid, r.RemoteAddr)

		res = dbsession.SetValues("db_session",
			map[string]string{"uid": "0", "closed": "0", "sid": sid, "remote":  hostOnly(r.RemoteAddr)},
			map[string]string{})

		if res.Err != nil {
			responceError(&w, "DbOpen open session", res.Err.Error())
			return
		}

	} else {
		sid = res.Values[0]
	}

	var json string

	json = "{"
	json += "\"Sid\" : \"" + sid + "\""
	json += "}"

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	io.WriteString(w, json+"\n")
}

func handleDbClose(w http.ResponseWriter, r *http.Request) {
	var t map[string]string //json.RawMessage
	var p []byte
	var err error

	p, err = ioutil.ReadAll(r.Body)

	if err != nil {
		responceError(&w, "DbClose body read", err.Error())
		return
	}

	log.Printf("BODY: %q", p)

	err = json.Unmarshal(p, &t)

	if err != nil {
		responceError(&w, "DbClose unmarshal", err.Error())
		return
	}

	sid, has := t["Sid"]

	if has == false {
		responceError(&w, "DbClose session", "No session id")
		return
	}

	log.Printf("Sid: %s", sid)

	//keys := fmt.Sprintf("sid=%s,closed=0", sid)

	res := dbsession.HasValues("db_session", []string{}, map[string]string{"sid": sid, "closed": "0"})

	if res.Err != nil {
		responceError(&w, "DbClose find session", res.Err.Error())
		return
	}

	res = dbsession.SetValues("db_session", map[string]string{"closed": "1"}, map[string]string{"sid": sid, "closed": "0"})

	if res.Err != nil {
		responceError(&w, "DbClose close session", res.Err.Error())
		return
	}

	var json string

	json = "{"
	json += "\"Closed\" : \"True\""
	json += "}"

	io.WriteString(w, json+"\n")
}

func handleDbValid(w http.ResponseWriter, r *http.Request) {
	var t map[string]string
	var p []byte
	var err error

	p, err = ioutil.ReadAll(r.Body)

	if err != nil {
		responceError(&w, "DbValid body read", err.Error())
		return
	}

  log.Println("Check db valid body:", string(p))

	err = json.Unmarshal(p, &t)

	if err != nil {
		responceError(&w, "DbValid unmarshal", err.Error())
		return
	}

	sid, has := t["sid"]

	if has == false {
		responceError(&w, "DbValid session", "No session id")
		return
	}

	log.Printf("Sid: %s", sid)

	res := dbsession.HasValues("db_session", []string{}, map[string]string{"sid": sid, "closed": "0", "remote" : hostOnly(r.RemoteAddr)})

	if res.Err != nil {
		responceError(&w, "DbValid find session", res.Err.Error())
		return
	}

	var json string

	json = "{"
	json += "\"Valid\" : \"True\""
	json += "}"

	io.WriteString(w, json+"\n")
}

func handleValGet(w http.ResponseWriter, r *http.Request) {
	var t KGetValues
	var p []byte
	var err error

	p, err = ioutil.ReadAll(r.Body)
	if err != nil {
		responceError(&w, "GetValues read body", err.Error())
		return
	}

  if !isSessionValid(getCookie(r, "dbsid"), hostOnly(r.RemoteAddr)) {
		responceError(&w, "GetValues read body", "Invalid session.")
		return
  }

	err = json.Unmarshal(p, &t)

	if err != nil {
		responceError(&w, "GetValues unmarshal", err.Error())
		return
	}

	log.Printf("Container: %s", t.Container)
	log.Printf("Vals: %d", len(t.Values))
	log.Printf("Keys: %d", len(t.Keys))

	res := dbsession.GetValues(t.Container, t.Values, t.Keys)

	if res.Err != nil {
		responceError(&w, "GetValues db request", res.Err.Error())
		return
	}

	var json string

	json = "["
	for i, v := range res.Values {
		json += "\"" + v + "\""

		if i != (len(res.Values) - 1) {
			json += ", "
		}
	}
	json += "]"

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	io.WriteString(w, json+"\n")

  log.Println("GetValues write body: ", json)
}

func handleValSet(w http.ResponseWriter, r *http.Request) {
	var t KSetValues
	var p []byte
	var err error

	p, err = ioutil.ReadAll(r.Body)
	if err != nil {
		responceError(&w, "SetValues read body", err.Error())
		return
	}

  if !isSessionValid(getCookie(r, "dbsid"), hostOnly(r.RemoteAddr)) {
		responceError(&w, "GetValues read body", "Invalid session.")
		return
  }

	err = json.Unmarshal(p, &t)

	if err != nil {
		responceError(&w, "SetValues unmarshal", err.Error())
		return
	}

	log.Printf("Container: %s", t.Container)
	log.Printf("Vals: %d", len(t.Values))
	log.Printf("Keys: %d", len(t.Keys))

	res := dbsession.SetValues(t.Container, t.Values, t.Keys)

	if res.Err != nil {
		responceError(&w, "SetValues sql request", res.Err.Error())
		return
	}

	var json string

	json = "{}"

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	io.WriteString(w, json+"\n")
}

func handleValDel(w http.ResponseWriter, r *http.Request) {
	var t KDelValues
	var p []byte
	var err error

	p, err = ioutil.ReadAll(r.Body)
	if err != nil {
		responceError(&w, "DelValues read body", err.Error())
		return
	}

  if !isSessionValid(getCookie(r, "dbsid"), hostOnly(r.RemoteAddr)) {
		responceError(&w, "GetValues read body", "Invalid session.")
		return
  }

	err = json.Unmarshal(p, &t)

	if err != nil {
		log.Println("Del_Values_Error: ", err)
		responceError(&w, "DelValues unmarshal", err.Error())
		return
	}

	log.Printf("Container: %s", t.Container)
	log.Printf("Vals: %d", len(t.Values))
	log.Printf("Keys: %d", len(t.Keys))

	res := dbsession.DelValues(t.Container, t.Values, t.Keys)

	if res.Err != nil {
		log.Println("DB Del_Values_Error: ", res.Err)

		responceError(&w, "DelValues sql request", res.Err.Error())
		return
	}

	var json string

	json = "{}"

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	io.WriteString(w, json+"\n")
}

func handleValHas(w http.ResponseWriter, r *http.Request) {
	var t KHasValues
	var p []byte
	var err error

	p, err = ioutil.ReadAll(r.Body)
	if err != nil {
		responceError(&w, "HasValues read body", err.Error())
		return
	}

  if !isSessionValid(getCookie(r, "dbsid"), hostOnly(r.RemoteAddr)) {
		responceError(&w, "GetValues read body", "Invalid session.")
		return
  }

	err = json.Unmarshal(p, &t)

	if err != nil {
		responceError(&w, "HasValues unmarshal", err.Error())
		return
	}

	log.Printf("Container: %s", t.Container)
	log.Printf("Vals: %d", len(t.Values))
	log.Printf("Keys: %d", len(t.Keys))

	res := dbsession.HasValues(t.Container, t.Values, t.Keys)

	if res.Err != nil {
		responceError(&w, "HasValues sql request", res.Err.Error())
		return
	}

	var json string

	json = "{}"

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	io.WriteString(w, json+"\n")
}

func initDB(dir string) {

}

var Monitor bool = true

func monitor() {
	log.Println("start monitoring... %", Monitor)

	for Monitor == true {
		time.Sleep(1000 * time.Millisecond)
	}
}

func main() {
	port := os.Getenv("PORT	")

	if port == "" {
		port = "5100"
	}

	var cfg Config

	err := gcfg.ReadFileInto(&cfg, "main.conf")

	if err != nil {
		log.Println("Error: ", err)

		panic(1)
	} else {
		log.Println("DBPath: ", cfg.Main.DbPath)
		log.Println("debug: ", cfg.Main.Debug)
	}
	resdebug = cfg.Main.Debug
	dbsession = ksqlttp.NewDbSession(cfg.Main.DbPath)

	if dbsession == nil {
		log.Println("Error: Unable create database session. ", err)

		return
	}

	// capture ctrl+c and stop CPU profiler
	ctrl_c := make(chan os.Signal, 1)
	signal.Notify(ctrl_c, os.Interrupt)
	go func() {
		for sig := range ctrl_c {
			log.Printf("Got %v, Exiting..", sig)
			dbsession.Close()
			pprof.StopCPUProfile()
			os.Exit(1)
		}
	}()

	log.Println("Activate port: ", port)

	http.HandleFunc("/", handleIndex)
	http.HandleFunc("/dbopen", handleDbOpen)
	http.HandleFunc("/dbclose", handleDbClose)
	http.HandleFunc("/dbvalid", handleDbValid)
	http.HandleFunc("/valuesget", handleValGet)
	http.HandleFunc("/valuesset", handleValSet)
	http.HandleFunc("/valuesdel", handleValDel)
	http.HandleFunc("/valueshas", handleValHas)

	go monitor()

	//err = http.ListenAndServe(":"+port, nil)
	err = http.ListenAndServeTLS(":"+port, cfg.Main.Tlscert, cfg.Main.Tlskey, nil)

	Monitor = false

	if err != nil {
		log.Println("Error: ", err)
	}

	dbsession.Close()

	log.Println("Finish.")
}
